console.log("Tugas 4 Function")
console.log("=== Nomor 1 ===")
console.log()

function teriak(){
    return " Halo Sanbers!"
}console.log(teriak())
console.log()
console.log("=== Nomor 2 ===")
console.log()

function kalikan(x, y){
    return x*y
}
var num1 = 12
var num2 = 4

var hasil = kalikan(num1, num2)
console.log("Hasil dari perkalian angka1 dan angka2 adalah :", hasil)
console.log()
console.log("=== Nomor 3 ===")
console.log()

function introduce(name, age, address, hobby){
    var kalimat = "Nama saya adalah " + name + " usia saya " + age + " saya tinggal di " + address + " dan juga hobby saya adalah " + hobby + "!"
    return kalimat
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 