console.log("Tugas 5 Array")
console.log("=== Nomor 1 ===")
console.log()

function range(angka1, angka2){
    var arr = []
    if (angka1<=angka2){ 
        for (var i=angka1; i<=angka2; i++){
            arr.push(i)
        }
    } else if(angka1>=angka2){
        for(var i=angka1; i>=angka2; i--){
            arr.push(i)
        }
    } else{
        arr.push(-1)
    }
    return arr
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log()
console.log("=== Nomor 2 ===")
console.log()

function range1(angka1, angka2, step){
    var arr = []
    if (angka1<=angka2){ 
        for (var i=angka1; i<=angka2; i += step){
            arr.push(i)
        }
    } return arr
}

console.log(range1(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(range1(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(range1(5, 2, 1)) // [5, 4, 3, 2]
console.log(range1(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log()
console.log("=== Nomor 3 ===")
console.log()

function sum(angka1, angka2, step){
    var arr = []
    if (angka1<=angka2){ 
        for (var i=angka1; i<=angka2; i+=step){
            arr.push(i)
        }
    } return arr
}
console.log(sum(1,10,1))