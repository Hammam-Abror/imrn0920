var nama = "john"
var peran = ""
console.log("\t======Tugas if-else======");
console.log();

if ( nama == " " && peran == " ") {
    console.log("Nama harus diisi!")  
    } else if( nama == "john" && peran == "") {
        console.log("Halo John, Pilih peranmu untuk memulai game!")
    } else if ( nama == "jane" && peran == "penyihir") {
        console.log("Selamat datang di Dunia Werewolf, Jane")
        console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
    } else if ( nama == "jaenita" && peran == "guard") {
        console.log("Selamat datang di Dunia Werewolf, Jenita")
        console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
    } else if ( nama == "junaedi" && peran == "werewolf") {
        console.log("Selamat datang di Dunia Werewolf, Junaedi")
        console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
    
} else {
    console.log("error")
}
console.log();
console.log("\t======Tugas switch-case======");
console.log();

var hari = 21; 
var bulan = 1; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945';
switch(bulan){
    case 1: {console.log(hari + ' januari ' + tahun); break; }
    default:  { console.log('Tidak terjadi apa-apa'); }
}
//console.log(hari + ' januari ' + tahun);